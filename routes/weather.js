var async = require('async'),
    weather = require("openweathermap");

weather.defaults = {
    "units": "metric",
    "lang": "en",
    "mode": "json"
};

function getLocationsNearby(lat, lon, callback) {
    var arguments = {
        lat: lat,
        lon: lon,
        cn: 10,
        units: 'metric'
    };

    weather.find(arguments, function (json) {
        callback(null, json.list);
    });
};

exports.summary = function (req, res) {
    if (req.query.lat && req.query.lon) {
        getLocationsNearby(req.query.lat, req.query.lon, function (err, nearby) {
            res.render('summary', {
                nearby: nearby
            });
        });
    } else {
        res.render('summary-redirect');
    }
};

/*
 * GET weather for a specific location
 */
exports.location = function (req, res) {
    var arguments = {"q": req.params.city, "cnt": 2, "units": "metric"};

    weather.find(arguments, function (json) {
        res.send(json);
    });
};

exports.search = function (req, res) {
    console.log(req.params);
    if (Object.keys(req.query).length === 0 || !req.query.location) {
        res.render('search', {cities: []});
    } else {
        var arguments = {q: req.query.location, cnt: 2};

        weather.find(arguments, function (json) {
            console.log(json);
            res.render('search', {cities: json.list});
        });
    }
};

exports.details = function (req, res) {
    arguments = {id: req.params.id, cnt: 7, units: "metric"}

    weather.daily(arguments, function(json) {
        console.log(json.list[0]);

        var viewArguments = {
            city: json.city,

            days: json.list.map(function(day) {
                console.log(day);
                var date = new Date(day.dt * 1000);
                return {
                    date: date.getFullYear() + "-" + date.getMonth() + "-" + date.getDay(),
                    temp: day.temp,
                    current: day.deg,
                    iconUrl: day.weather[0].iconUrl,
                    description: day.weather[0].main,
                    text: day.weather[0].description
                };
            })
        }

        res.render('details', viewArguments)
    });
};