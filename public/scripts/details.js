$(function () {
    var name = $('.main-navigation h1').text(),
        urlParts = document.URL.split('/'),
        id = urlParts[urlParts.length - 1],
        location = {name: name, id: id};
    saveLocationToHistory(location);
});