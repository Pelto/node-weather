function getLocationHistory() {
    var json = amplify.store('locationHistory');
    if(!json) {
        json = "[]";
    }
    return JSON.parse(json);
}

function saveLocationToHistory(location) {
    var history = getLocationHistory(),
        alreadySaved = false;

    for (i = 0; i < history.length && !alreadySaved; i++) {
        if (history[i].id === location.id) {
            alreadySaved = true;
        }
    }

    if (!alreadySaved) {
        history.push(location);
    }

    amplify.store('locationHistory', JSON.stringify(history));
}