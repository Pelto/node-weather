$(function () {
    var $history = $('.history'),
        history = getLocationHistory().slice(0, 9);

    if(history.length) {
        $sectionTitle = $('<article></article>');
        $sectionTitle.addClass('section-title');
        $sectionTitle.addClass('row');
        $sectionTitle.text('History');

        $history.append($sectionTitle);

        $inHistory = $('<article></article>');
        $inHistory.addClass('row');
        $inHistory.addClass('in-history');

        $.each(history, function(i, location) {

            $link = $('<a></a>');
            $link.attr('href', '/locations/' + location.id);

            $title = $('<h2></h2>');
            $title.addClass('name');
            $title.text(location.name);

            $link.append($title);

            $inHistory.append($link);
        });

        $history.append($inHistory);
    }
});