function getGeoLocation(callback) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            // on success
            function (position) {
                console.dir(position);
                callback(null, position);
            },
            // on error
            function (error) {
                switch (error.code) {
                    case error.PERMISSION_DENIED:
                        callback("User denied the request for Geolocation.", null)
                        break;
                    case error.POSITION_UNAVAILABLE:
                        callback("Location information is unavailable.", null)
                        break;
                    case error.TIMEOUT:
                        callback("The request to get user location timed out.", null)
                        break;
                    default:
                        callback("An unknown error occurred.", null)
                        break;
                }
            });
    }
    else {
        callback('Geo location is not available', null);
    }
}

function getIPLocation(callback) {
    $.get('http://freegeoip.net/json/')
        .done(function (data) {
            callback(null, {
                coords: {
                    longitude: data.longitude,
                    latitude: data.latitude
                }
            });
        })
        .fail(function () {
            callback('Failed to get location by IP', null);
        });
}

function getLocation(callback) {
    getGeoLocation(function (err, location) {
        if (err) {
            // fallback
            getIPLocation(callback);
        } else {
            callback(err, location);
        }

    });
}